#### About
This is a module for managing Config of application

Input : Properties

Function : Load Config properties and use it

#### Usage
1.  Load Properties using ```ConfigManager.load(Properties configs)``` 
2.  Read Loaded config by using ```ConfigHelper.getString(String key)``` or ```ConfigHelper.getInt(String key)``` or ```ConfigHelper.getDouble(String key)```

#### Contact Developer
ffaraniarijaona@gmail.com