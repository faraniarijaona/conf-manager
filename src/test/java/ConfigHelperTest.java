import oma.intern.config.ConfigHelper;
import org.junit.Assert;
import org.junit.Test;

/*
 *Date : 14/08/2020 14:08:2020
 *User : faf_ext
 */
public class ConfigHelperTest {

    @Test
    public void testGetDate(){
        new ConfigManagerTest().testLoadAndGet();

        Assert.assertEquals(80, ConfigHelper.getInt("port", 0));
        Assert.assertEquals("localhost", ConfigHelper.getString("host", "0.0.0.0"));
        Assert.assertEquals(36.5, ConfigHelper.getDouble("temperature", 34.08), 0.001);
    }
}
