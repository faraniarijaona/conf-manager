import oma.intern.config.ConfigManager;
import org.junit.Assert;
import org.junit.Test;

import java.util.Properties;

/*
 *Date : 14/08/2020 14:08:2020
 *User : faf_ext
 */
public class ConfigManagerTest {

    @Test
    public void testLoadAndGet(){
        Properties prop = new Properties();
        prop.put("port", 80);
        prop.put("host", "localhost");

        ConfigManager.load(prop);

        Assert.assertEquals(2, ConfigManager.getMap().size());

        prop.put("scheme", "http");
        prop.put("temperature", 36.5);
        ConfigManager.load(prop);
        Assert.assertEquals(4, ConfigManager.getMap().size());
    }
}
