package oma.intern.config;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.locks.ReentrantLock;

/*
 *Date : 13/08/2020 13:08:2020
 *User : faf_ext
 */
public class ConfigManager {
    private static final ReentrantLock locker = new ReentrantLock();

    private static final Map<String, Object> map = new HashMap<>();

    public static void load(Properties  properties){
        locker.lock();
        try {
            map.clear();
            properties.forEach((key, value) -> map.put(key.toString(), value));
        }
        finally {
            locker.unlock();
        }
    }

    public static Map<String, Object> getMap(){
        return Collections.unmodifiableMap(map);
    }
}
