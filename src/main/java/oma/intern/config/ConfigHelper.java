package oma.intern.config;

import java.util.Map;

/*
 *Date : 13/08/2020 13:08:2020
 *User : faf_ext
 */
public class ConfigHelper {

    private static Map<String, Object> confs(){
        return ConfigManager.getMap();
    }

    public static String getString(String key){
        return System.getenv(key) == null ? String.valueOf(confs().get(key)) : System.getenv(key);
    }

    public static String getString(String key, String defaultValue){
        return confs().containsKey(key)? System.getenv(key) == null ? String.valueOf(confs().get(key)) : System.getenv(key) : defaultValue;
    }

    public static int getInt(String key){
        return  System.getenv(key) == null ? Integer.parseInt(confs().get(key).toString()) : Integer.parseInt(System.getenv(key));
    }

    public static int getInt(String key, int defaultValue){
        return confs().containsKey(key)? System.getenv(key) == null ? (int)confs().get(key) : Integer.parseInt(System.getenv(key)) : defaultValue;
    }

    public static double getDouble(String key){
        return System.getenv(key) == null ? (double)confs().get(key) : Double.parseDouble(System.getenv(key));
    }

    public static double getDouble(String key, double defaultValue){
        return confs().containsKey(key)? System.getenv(key) == null ? (double)confs().get(key) : Double.parseDouble(System.getenv(key)) : defaultValue;
    }

    public static  boolean containsKey(String key){
        return confs().containsKey(key);
    }
}
